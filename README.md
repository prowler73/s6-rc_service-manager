# s6-rc_service-manager

--Recommended build instructions

`gbp clone https://gitlab.com/ProwlerGr/s6-rc_service-manager.git && cd s6-rc_service-manager && gbp buildpackage -uc -us`
is the recommended method to build this package directly from git.

`sudo apt install git-buildpackage lowdown` 
should get you all the software required to build using this method
