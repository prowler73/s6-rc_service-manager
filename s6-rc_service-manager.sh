#!/bin/bash

############### s6-rc SERVICE MANAGER #################
#### Manage s6-rc services with a simple graphical ####
####   interface. Add, remove, start or stop any   ####
####       available s6-rc service and log.        ####
####   # Initially developed for antiX linux #     ####
#######################################################

### Localization ###
TEXTDOMAINDIR=/usr/share/locale
TEXTDOMAIN=s6-rc-service-manager

if [[ ! -e /usr/bin/dialogbox ]]; then
	yad --width=250 --height=75 --fixed --title="s6-rc Service Manager" --button Exit --info --text "Dialogbox is not installed. Application will exit."
    echo "dialogbox is NOT installed. Cannot start the script." 
    exit 1
fi

if [[ $EUID -eq 0 ]]; then
    echo "RUNNING AS ROOT"
else
	yad  --width=250 --height=75 --fixed --title="s6-rc Service Manager" --button Exit --info --text "This application needs to be run as root. Application will exit."
    echo "NOT RUNNING AS ROOT"
    exit 1
fi

if [[ $(ps -p1 | grep -ic "s6-svscan") -eq 0 ]]; then
	yad --width=250 --height=75 --fixed --title="s6-rc Service Manager" --button Exit --info --text "s6-svscan is not running as PID 1. Application will exit."
    echo "s6-svscan is not PID 1"
    exit 1
fi

if [[ ! -e /run/s6-rc ]]; then
	yad --width=250 --height=75 --fixed --title="s6-rc Service Manager" --button Exit --info --text "s6-rc is not managing services. Application will exit."
    echo "s6-rc is NOT managing services" 
    exit 1
fi

### Files and folders ###
SERVICE_SCANDIR="/run/s6-rc/scandir"
SERVICE_DIRECTORY="/etc/s6-rc/compiled/servicedirs"
SERVICE_SOURCE="/etc/s6-rc/sv"
SERVICE_STATUS_LIST="$(mktemp -p /dev/shm)"
SERVICE_STARTUP_LIST="$(mktemp -p /dev/shm)"
VITAL_SERVICES_LIST="$(mktemp -p /dev/shm)"
SERVICES_UP_LIST="$(mktemp -p /dev/shm)"
SERVICES_DOWN_LIST="$(mktemp -p /dev/shm)"
SERVICES_SVSTAT_LIST="$(mktemp -p /dev/shm)"
SERVICE_MANAGER_BUNDLE="antix-service-manager"
SERVICE_MANAGER_DIRECTORY="$SERVICE_SOURCE/$SERVICE_MANAGER_BUNDLE/contents.d"

ALL_SERVICES="$(ls -1 "$SERVICE_SOURCE")"

### MAIN ICONS ###
ICONS="/usr/share/icons/papirus-antix/48x48"
FALLBACK_ICONS="/usr/share/icons/Adwaita/48x48"
ICON_SIZE="48px"
MAIN_WINDOW_ICON="$ICONS/devices/server-database.png"
MAIN_WINDOW_ICON="${MAIN_WINDOW_ICON:-$FALLBACK_ICONS/devices/computer.png}"
SERVICE_VITAL_ICON="$FALLBACK_ICONS/status/changes-prevent-symbolic.symbolic.png"
SERVICE_UP_ICON="$FALLBACK_ICONS/ui/checkbox-checked-symbolic.symbolic.png"
SERVICE_DOWN_ICON="$FALLBACK_ICONS/ui/window-close-symbolic.symbolic.png"
SERVICE_UNUSED_ICON="$FALLBACK_ICONS/ui/checkbox-symbolic.symbolic.png"

### STRINGS OF TEXT ###
MAIN_WINDOW_TITLE=$"s6-rc Service Manager"
SERVICES_TEXT=$"Services"
SERVICE_TEXT=$"Service"
LOG_TEXT=$" "
ADD_SERVICE_TEXT=$"Add unused service"
VITAL_WARNING_TEXT=$"This is a VITAL service (it cannot be disabled)"
STATUS_TEXT=$"Status:"
STARTUP_TEXT=$"Startup:"
UP_STATUS_TEXT=$"Up"
DOWN_STATUS_TEXT=$"Down"
YES_STARTUP_TEXT=$"Yes"
NO_STARTUP_TEXT=$"No"
START_BUTTON_TEXT=$"Start"
RESTART_BUTTON_TEXT=$"Restart"
STOP_BUTTON_TEXT=$"Stop"
ENABLE_BUTTON_TEXT=$"Enable"
DISABLE_BUTTON_TEXT=$"Disable"
ADD_BUTTON_TEXT=$"Add"
REMOVE_BUTTON_TEXT=$"Remove"
RELOAD_BUTTON_TEXT=$"Reload"


cleanup() {
    ### Remove temporary files
    rm -f -- "$SERVICE_STATUS_LIST"
    rm -f -- "$SERVICE_STARTUP_LIST"
    rm -f -- "$VITAL_SERVICES_LIST"
    rm -f -- "$SERVICES_UP_LIST"
    rm -f -- "$SERVICES_DOWN_LIST"
    rm -f -- "$SERVICES_SVSTAT_LIST"
    echo "Cleanup and exiting"
}


### RECOMPILE s6-rc DATABASE ###
refresh_s6-db(){
    s6-rc-update /etc/s6-rc/compiled-release
	mv /etc/s6-rc/compiled-active /etc/s6-rc/compiled-superseded-$(date +%s%N)
	rm -r /etc/s6-rc/compiled
	s6-rc-compile /etc/s6-rc/compiled-active /etc/s6-rc/sv
	ln -sf /etc/s6-rc/compiled-active /etc/s6-rc/compiled
	s6-rc-update /etc/s6-rc/compiled
	rm -rf /etc/s6-rc/compiled-superseded*	
}


### UPDATE THE LISTS OF SERVICES IN VARIOUS STATUSES ###
generate_service_lists() {
	local SERVICE_NAME
    local SERVICE_STARTUP
    local SERVICE_STATUS
    local LOG_STATUS

### ALL UNCOMPILED SERVICE FILES   
ALL_SERVICES="$(ls -1 "$SERVICE_SOURCE")"
    
### STARTUP SERVICES (BUNDLES "default" + "boot" + ALL THEIR CONTENTS)
echo "s6rc-oneshot-runner
s6rc-fdholder
default" > "$SERVICE_STARTUP_LIST"
echo "$(ls -1 $SERVICE_SOURCE/default/contents.d)" >> "$SERVICE_STARTUP_LIST"
echo "$(ls -1 $SERVICE_SOURCE/boot/contents.d)" >> "$SERVICE_STARTUP_LIST"
echo "$(ls -1 $SERVICE_SOURCE/$SERVICE_MANAGER_BUNDLE/contents.d)" >> "$SERVICE_STARTUP_LIST"
STARTUP_SERVICES="$(cat $SERVICE_STARTUP_LIST)"  
    # Save SERVICE_STARTUP_LIST in correct order
    echo "$STARTUP_SERVICES" | awk '{print $1 " " $2 " " $3}' > "$SERVICE_STARTUP_LIST"

### VITAL SERVICES & BUNDLES (EVERYTHING THAT WON'T BE CONTROLLED BY SERVICE MANAGER)
echo "$(ls -1 /etc/s6-rc/sv/boot/contents.d)" > "$VITAL_SERVICES_LIST"
echo "s6rc-oneshot-runner
s6rc-fdholder
default
boot
antix-service-manager" >> "$VITAL_SERVICES_LIST"
VITAL_SERVICES="$(cat $VITAL_SERVICES_LIST)"

### SERVICES DOWN
echo "$(s6-rc -da list)" > "$SERVICES_DOWN_LIST"
SERVICES_DOWN="$(cat $SERVICES_DOWN_LIST)"

### SERVICES UP
echo "$(s6-rc -ua list)" > "$SERVICES_UP_LIST"
SERVICES_UP="$(cat $SERVICES_UP_LIST)"

### SERVICES SVSTAT
echo "$(cd /run/s6-rc/scandir && for i in *; do printf "$i: `s6-svstat  $i`\n" | sed 's/://g' >> "$SERVICES_SVSTAT_LIST"; done)"
SERVICES_SVSTAT="$(cat $SERVICES_SVSTAT_LIST)"
	# Save SERVICE_SVSTAT_LIST in correct order
    echo "$SERVICES_SVSTAT" | awk '{print $1 " " $2}' > "$SERVICES_SVSTAT_LIST"
}

### Set trap on EXIT for cleanup
trap cleanup EXIT

### FORCE S6-RC TO RESCAN FOR AVAILABLE SERVICES
s6-rc_force_rescan(){
    sudo kill -SIGALRM 1 && sleep 1.5s
}

reload_service_listbox(){
    # Do NOT regenerate the list if the services list file is empty
    if [[ ! -s "$SERVICE_STARTUP_LIST" ]]; then return 1; fi
    
    local TEMP_LAYOUT=$(mktemp -p /dev/shm)
    local APROPIATE_ICON
    local SERVICE_NAME
    
    # clear current Service Listbox items and position
    echo "clear SERVICE_LISTBOX" >> "$TEMP_LAYOUT"
    echo "position onto SERVICE_LISTBOX" >> "$TEMP_LAYOUT"
    
    # Process each service at a time
    while read -r line; do
        APROPIATE_ICON=""
        SERVICE_NAME="$(echo "$line" | awk '{print $1}')"
        
        # If this service is vital, use apropriate icon
        if [[ $(echo "$VITAL_SERVICES" | grep -c "^${SERVICE_NAME}$") -gt 0 ]]; then
            APROPIATE_ICON="$SERVICE_VITAL_ICON"
        # If this service is up, use apropriate icon
        elif [[ $(echo "$SERVICES_UP" | grep -c "^${SERVICE_NAME}$") -gt 0 ]]; then
            APROPIATE_ICON="$SERVICE_UP_ICON"
        # If this service is down, use the apropriate icon
         elif [[ $(echo "$SERVICES_DOWN" | grep -c "^${SERVICE_NAME}$") -gt 0 ]]; then
            APROPIATE_ICON="$SERVICE_DOWN_ICON"
        fi
        echo "add item \"$SERVICE_NAME\" $APROPIATE_ICON" >> "$TEMP_LAYOUT"
    done < "$SERVICE_STARTUP_LIST"
    
    
    # Return at the end of the file
    echo "position behind BOTTOM_FRAME" >> "$TEMP_LAYOUT"
    
    # Reposition list selection
    if [[ ! -z "$SERVICE_SELECTED" ]] && [[ $(grep -ic "^${SERVICE_SELECTED} " "$SERVICE_STARTUP_LIST") -gt 0 ]]; then
        echo "set \"SERVICE_LISTBOX:$SERVICE_SELECTED\" current" >> "$TEMP_LAYOUT"
    fi
    
    # Export changes to dialogbox
    cat "$TEMP_LAYOUT" >&$OUTPUTFD
    
rm -f -- "$TEMP_LAYOUT"
}

# Check the service status
get_service_status(){
    local SELECTED_SERVICE="${1}"
    # Do nothing is no service selected
    if [[ -z "$SELECTED_SERVICE" ]]; then return 1; fi
    
    local TEMP_LAYOUT=$(mktemp -p /dev/shm)
    
    local SERVICE_ITEM="$(cat "$SERVICES_SVSTAT_LIST" | grep "^${SELECTED_SERVICE} ")"
    local SERVICE_STATUS="$(echo $SERVICE_ITEM | awk '{print $2}')"
    local LOG_STATUS="$(echo $SERVICE_ITEM | awk '{print $3}')"
    local SERVICE_STARTUP
    local LOG_STARTUP
    
    # Display status info
    echo "set SERVICE_NAME_TEXT text \"<big><b>${SELECTED_SERVICE}</b></big>\"" >> $TEMP_LAYOUT
    
    # Enable all buttons
    echo "hide START_SERVICE_BUTTON" >> $TEMP_LAYOUT
    echo "hide RESTART_SERVICE_BUTTON" >> $TEMP_LAYOUT
    echo "hide STOP_SERVICE_BUTTON" >> $TEMP_LAYOUT
    echo "hide START_LOG_BUTTON" >> $TEMP_LAYOUT
    echo "hide STOP_LOG_BUTTON" >> $TEMP_LAYOUT
    echo "hide ENABLE_SERVICE_BUTTON" >> $TEMP_LAYOUT
    echo "hide DISABLE_SERVICE_BUTTON" >> $TEMP_LAYOUT
    echo "hide ENABLE_LOG_BUTTON" >> $TEMP_LAYOUT
    echo "hide DISABLE_LOG_BUTTON" >> $TEMP_LAYOUT
    
    # Display service status information
    case "$SERVICE_STATUS" in
        up)
            SERVICE_STATUS="<b>${STATUS_TEXT}</b> $UP_STATUS_TEXT"
            echo "show STOP_SERVICE_BUTTON" >> $TEMP_LAYOUT
            echo "show RESTART_SERVICE_BUTTON" >> $TEMP_LAYOUT
            echo "enable STOP_SERVICE_BUTTON" >> $TEMP_LAYOUT;;
        down)
            SERVICE_STATUS="<b>${STATUS_TEXT}</b> $DOWN_STATUS_TEXT"
            echo "show START_SERVICE_BUTTON" >> $TEMP_LAYOUT
            echo "enable START_SERVICE_BUTTON" >> $TEMP_LAYOUT;;
        *)
            SERVICE_STATUS="<b>${STATUS_TEXT}</b> not LongRun"
            echo "show START_SERVICE_BUTTON" >> $TEMP_LAYOUT
            echo "enable START_SERVICE_BUTTON" >> $TEMP_LAYOUT;;
    esac
    echo "set SERVICE_STATUS_TEXT text \"$SERVICE_STATUS\"" >> $TEMP_LAYOUT
    
     # Get service startup information
    if [[ ! -f "${SERVICE_DIRECTORY}/${SELECTED_SERVICE}/down" ]]; then
        SERVICE_STARTUP="<b>${STARTUP_TEXT}</b> $YES_STARTUP_TEXT"
        echo "show DISABLE_SERVICE_BUTTON" >> $TEMP_LAYOUT
        echo "enable DISABLE_SERVICE_BUTTON" >> $TEMP_LAYOUT
    else
        SERVICE_STARTUP="<b>${STARTUP_TEXT}</b> $NO_STARTUP_TEXT"
        echo "show ENABLE_SERVICE_BUTTON" >> $TEMP_LAYOUT
        echo "enable ENABLE_SERVICE_BUTTON" >> $TEMP_LAYOUT
    fi
    echo "set SERVICE_STARTUP_TEXT text \"$SERVICE_STARTUP\"" >> $TEMP_LAYOUT
    
    # If the selected service is Vital, display the message and disable service buttons
    if [[ $(echo "$VITAL_SERVICES" | grep -c "^${SELECTED_SERVICE}$") -gt 0 ]]; then
        echo "show VITAL_WARNING_TEXT" >> $TEMP_LAYOUT
        echo "disable STOP_SERVICE_BUTTON" >> $TEMP_LAYOUT
        echo "disable DISABLE_SERVICE_BUTTON" >> $TEMP_LAYOUT
        echo "disable REMOVE_SERVICE_BUTTON" >> $TEMP_LAYOUT
    else
        echo "hide VITAL_WARNING_TEXT" >> $TEMP_LAYOUT
        echo "enable REMOVE_SERVICE_BUTTON" >> $TEMP_LAYOUT
    fi
    
    # Export changes to dialogbox
    cat "$TEMP_LAYOUT" >&$OUTPUTFD
rm -f -- "$TEMP_LAYOUT"
}

# Add unloaded services
load_service(){
    local SERVICE_NAME
    local LOADED_SERVICES
    local UNLOADED_SERVICES
    local U_SERVICE_CONTENT
    local SERVICE_BUTTONS
    
    LOADED_SERVICES="$(cat "$SERVICE_STARTUP_LIST" | awk '{print $1}')"
    UNLOADED_SERVICES="$(echo "$ALL_SERVICES" | grep -vxf <(echo "$LOADED_SERVICES"))"
    # If no service to load, tell user
    if [[ -z "$UNLOADED_SERVICES" ]]; then
        # Format dialogbox elements
        U_SERVICE_CONTENT=$"All services are already loaded."
        U_SERVICE_CONTENT="add label \"${U_SERVICE_CONTENT}\" SERVICE_MESSAGE_TEXT"
    # Display a list of services the user wants to load
    else
        # Format UNLOADED_SERVICES for dialogbox (including icon)
        UNLOADED_SERVICES="$(echo "$UNLOADED_SERVICES" | awk -v icon="$SERVICE_UNUSED_ICON" '{print "add item " $0 " " icon}')"
        # Format dialogbox elements
        U_SERVICE_CONTENT="add listbox \"$SERVICES_TEXT\" LIST_SERVICES activation
            $UNLOADED_SERVICES
            end listbox LIST_SERVICES"
        SERVICE_BUTTONS="add frame horizontal
            add stretch
            add pushbutton \"${ADD_BUTTON_TEXT}\" ADD_SERVICE_PUSHBUTTON apply exit
            end frame"
    fi

    SERVICE_NAME=$(dialogbox --hidden -r <<ADDSERVICE
    $U_SERVICE_CONTENT
    $SERVICE_BUTTONS
    set stylesheet " QFrame {min-width:9em;}
                     QListWidget {icon-size:18px; text-align:left;
                      min-width:7em; min-height:6em; padding:3px}"
    set title "$ADD_SERVICE_TEXT"
    set icon "$MAIN_WINDOW_ICON"
    
    show
ADDSERVICE
)

    if [ $? -ne 0 ]; then
        SERVICE_NAME="$(echo "$SERVICE_NAME" | grep "^LIST_SERVICES=" | tail -n1 | cut -d"=" -f2)"
        if [[ ! -z "$SERVICE_NAME" ]] && [[ -d "${SERVICE_SOURCE}/${SERVICE_NAME}" ]]; then
            echo "$ADD_BUTTON_TEXT - $SERVICE_NAME"
            sudo touch "$SERVICE_MANAGER_DIRECTORY/${SERVICE_NAME}"
            sudo refresh_s6-db 2>/dev/null && cleanup && generate_service_lists && reload_service_listbox
            SERVICE_SELECTED="$SERVICE_NAME"
            s6-rc_force_rescan
            sudo s6-svstat "$SERVICE_SCANDIR"/"$SERVICE_SELECTED" 1>/dev/null || sleep 1.5s
        fi
    fi
}

remove_service(){
    local SERVICE_NAME="${1}"
    
    if [[ ! -z "$SERVICE_NAME" ]] && [[ -e "${SERVICE_DIRECTORY}/${SERVICE_NAME}" ]]; then
        echo "$REMOVE_BUTTON_TEXT - $SERVICE_NAME"
        sudo rm "$SERVICE_MANAGER_DIRECTORY/${SERVICE_NAME}"
        sudo refresh_s6-db 2>/dev/null && cleanup && generate_service_lists && reload_service_listbox
        s6-rc_force_rescan
    fi
}

# Modify service status
change_service_status(){
    local SERVICE_ACTION="${1}"
    local SERVICE_NAME="${2}"
    local OUTPUT_MESSAGE
    local SERVICE_PATH="$SERVICE_NAME"

    # If no service is selected, get out of here
    if [[ -z "$SERVICE_NAME" ]]; then return 1; fi

    # Prepare output message
    case $SERVICE_ACTION in
        service_*) OUTPUT_MESSAGE="$SERVICE_NAME - $SERVICE_TEXT";;
            log_*) OUTPUT_MESSAGE="$SERVICE_NAME - $LOG_TEXT"
#                   SERVICE_PATH="${SERVICE_NAME}/log";;
    esac

    # perform correct action
    case $SERVICE_ACTION in
          *_start) echo "$OUTPUT_MESSAGE - $START_BUTTON_TEXT"
                   sudo s6-rc start $SERVICE_NAME && cleanup && generate_service_lists && reload_service_listbox ;;
        *_restart) echo "$OUTPUT_MESSAGE - $RESTART_BUTTON_TEXT"
                   sudo s6-rc stop $SERVICE_NAME && sudo s6-rc start $SERVICE_NAME && cleanup && generate_service_lists && reload_service_listbox ;;
           *_stop) echo "$OUTPUT_MESSAGE - $STOP_BUTTON_TEXT"
                   sudo s6-rc stop $SERVICE_NAME && cleanup && generate_service_lists && reload_service_listbox ;;
		*_enable) echo "$OUTPUT_MESSAGE - $ENABLE_BUTTON_TEXT"
                   sudo touch $SERVICE_MANAGER_DIRECTORY/$SERVICE_NAME && sudo refresh_s6-db && sudo s6-rc start $SERVICE_NAME && cleanup && generate_service_lists && reload_service_listbox ;;
        *_disable) echo "$OUTPUT_MESSAGE - $DISABLE_BUTTON_TEXT"
                   sudo rm $SERVICE_MANAGER_DIRECTORY/$SERVICE_NAME && sudo refresh_s6-db && sudo s6-rc stop $SERVICE_NAME && cleanup && generate_service_lists && reload_service_listbox ;;
esac
    
    # Recheck service status
    cleanup && generate_service_lists && reload_service_listbox
}

#~ main

################################
###### STARTING DIALOGBOX ######
################################

coproc dialogbox --hidden -r
INPUTFD=${COPROC[0]}  # file descriptor the dialogbox process writes to
OUTPUTFD=${COPROC[1]}  # file descriptor the dialogbox process reads from
DBPID=$COPROC_PID    # PID of the dialogbox, if you need it for any purpose... e.g. to kill it

set -o monitor  # Enable SIGCHLD

# Create the dialogbox
cat >&$OUTPUTFD <<S6-RC_SERVICES
set stylesheet " QPushButton {icon-size:$ICON_SIZE; min-width:5em;}
                 QLabel {qproperty-alignment: AlignCenter;}
                 QTabWidget::tab-bar {alignment: center;}
                 QTabWidget::pane {border: 0px solid black; background: transparent;}"

add frame LEFT_FRAME vertical noframe
    add listbox "<b>${SERVICES_TEXT}</b>" SERVICE_LISTBOX selection
    end listbox SERVICE_LISTBOX
    add pushbutton "$ADD_BUTTON_TEXT" ADD_SERVICE_BUTTON
    add pushbutton "$REMOVE_BUTTON_TEXT" REMOVE_SERVICE_BUTTON
    add pushbutton "$RELOAD_BUTTON_TEXT" RELOAD_SERVICE_BUTTON
end frame
set LEFT_FRAME stylesheet " QFrame {min-width:9em; max-width:12em;}
                            QPushButton {icon-size:18px}
                            QListWidget {icon-size:18px; text-align:left;
                              min-width:7em; max-width:11em;
                              padding:3px; min-height:14em}"

                                   
step horizontal
add separator SEPARATOR_1 vertical
step horizontal
add frame TITLE_FRAME horizontal noframe
    add stretch
    add label "<big><big><b>service</b></big></big>" SERVICE_NAME_TEXT
    add stretch
end frame
set TITLE_FRAME stylesheet "QLabel {min-width: 10em}"
add label "$VITAL_WARNING_TEXT" VITAL_WARNING_TEXT
hide VITAL_WARNING_TEXT

add stretch
add label "<b>${SERVICE_TEXT}</b>" SERVICE_TEXT

add frame SERVICE_STATUS_FRAME horizontal
    add label "$STATUS_TEXT ?" SERVICE_STATUS_TEXT
    add stretch
    add pushbutton "$START_BUTTON_TEXT" START_SERVICE_BUTTON
    set START_SERVICE_BUTTON stylesheet "min-width:1em"
    add pushbutton "$RESTART_BUTTON_TEXT" RESTART_SERVICE_BUTTON
    set RESTART_SERVICE_BUTTON stylesheet "min-width:1em"
    add pushbutton "$STOP_BUTTON_TEXT" STOP_SERVICE_BUTTON
    set STOP_SERVICE_BUTTON stylesheet "min-width:1em"
    disable START_SERVICE_BUTTON
    disable STOP_SERVICE_BUTTON
end frame

add frame SERVICE_STARTUP_FRAME horizontal
    add label "$STARTUP_TEXT ?" SERVICE_STARTUP_TEXT
    add stretch
    add pushbutton "$ENABLE_BUTTON_TEXT" ENABLE_SERVICE_BUTTON
    set ENABLE_SERVICE_BUTTON stylesheet "min-width:1em"
    add pushbutton "$DISABLE_BUTTON_TEXT" DISABLE_SERVICE_BUTTON
    set DISABLE_SERVICE_BUTTON stylesheet "min-width:1em"
    disable ENABLE_SERVICE_BUTTON
    disable DISABLE_SERVICE_BUTTON
end frame

set title "$MAIN_WINDOW_TITLE"
set icon "$MAIN_WINDOW_ICON"

show
S6-RC_SERVICES

SERVICE_SELECTED=""
generate_service_lists && reload_service_listbox

while IFS=$'=' read key value; do
  #echo "Output: $key $value"
  case $key in
## LEFT FRAME ##
    SERVICE_LISTBOX)
        SERVICE_SELECTED="$value"; get_service_status "$value";;
    RELOAD_SERVICE_BUTTON)
        echo "Reload Service list"
        generate_service_lists && reload_service_listbox;;
    ADD_SERVICE_BUTTON)
        load_service
        generate_service_lists && reload_service_listbox;;
    REMOVE_SERVICE_BUTTON)
        remove_service "$SERVICE_SELECTED"
        generate_service_lists && reload_service_listbox;;
## SERVICE ACTIONS ##
    START_SERVICE_BUTTON)
        change_service_status "service_start" "$SERVICE_SELECTED" ;;
    RESTART_SERVICE_BUTTON)
        change_service_status "service_restart" "$SERVICE_SELECTED" ;;
    STOP_SERVICE_BUTTON)
        change_service_status "service_stop" "$SERVICE_SELECTED" ;;
    ENABLE_SERVICE_BUTTON)
        change_service_status "service_enable" "$SERVICE_SELECTED" ;;
    DISABLE_SERVICE_BUTTON)
        change_service_status "service_disable" "$SERVICE_SELECTED" ;;    
    esac
done <&$INPUTFD

set +o monitor  # Disable SIGCHLD
wait $DBPID    # Wait for the user to complete the dialog
